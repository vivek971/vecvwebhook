import os
import smtplib
from email.message import  EmailMessage


EMAIL_ADDRESS = os.environ.get('EMAIL_ID')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASS')




msg = EmailMessage()
msg['Subject'] = 'Vecv Ticket generated details'
msg['From'] = EMAIL_ADDRESS
msg['To'] = 'vivek.sinha@globtier.co.in'
msg.set_content('Your ticket VECV-111 has been generated.')

with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
    smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)

    smtp.send_message(msg)