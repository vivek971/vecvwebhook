from flask import Flask, request
import json
import random
import os
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from datetime import datetime,date
import requests
import base64

app = Flask(__name__)

import mysql.connector as connector

conn = connector.connect(host='botgo-prod.cgacaa0ov6b8.ap-south-1.rds.amazonaws.com',
                         user='admin',
                         passwd='botgo123',
                         database='volvo')
my_cursor = conn.cursor()
# my_cursor.execute("CREATE TABLE vecv1(`User Id` VARCHAR(50),`Level 1`VARCHAR(150),`Level 2` VARCHAR(150),`Level 3` VARCHAR(150),`Level 4` VARCHAR(150),`Priority` VARCHAR(50),`Contact Name`VARCHAR(50),`Mobile No` VARCHAR(50),`Description` VARCHAR(50),`Details` VARCHAR(200),`File Uploaded` VARCHAR(50), `Files` LONGBLOB,`Ticket Id` VARCHAR(50), `Status`VARCHAR(50), `Created Date` VARCHAR(50),`Created Time`VARCHAR(50))")
x1 = []
li = []
tkt = []
id_name = {}
id_mail = {}
id_pass = {}
lev_pri = {}
id_det = {}
id_des = {}
id_cre = {}
id_tkt = []
file_lis = []
tkt_stat = {}

# get dict of id and name & id and email from table
my_cursor.execute("SELECT * FROM allD")
my_result = my_cursor.fetchall()
for x in my_result:
    a = {f'{x[0]}': f'{x[5]}'}
    b = {f'{x[0]}': f'{x[10]}'}
    id_name.update(a)
    id_mail.update(b)
# get dict of id and pass
my_cursor.execute("SELECT * FROM cred")
my_res = my_cursor.fetchall()
for x in my_res:
    a = {f'{x[0]}': f'{x[1]}'}
    id_pass.update(a)
id_s = list(id_pass.keys())

# get dict of level and priority
my_cursor.execute("SELECT * FROM priorityy")
my_re = my_cursor.fetchall()
for x in my_re:
    a = {f'{x[0].upper()}': f'{x[1]}'}
    lev_pri.update(a)

## get tickets already created
my_cursor.execute("SELECT * FROM vecv1")
my_tkt = my_cursor.fetchall()
for x in my_tkt:
    tkt.append(x[14])
    u_t = {f'{x[0]}': f'{x[12]}'}
    i_d = {f'{x[12]}': f'{x[8]}'}
    i_de = {f'{x[12]}': f'{x[9]}'}
    i_dt = {f'{x[12]}': f'{x[14]}'}

    id_tkt.append(u_t)
    id_det.update(i_d)
    id_des.update(i_de)
    id_cre.update(i_dt)


my_cursor.execute("SELECT * FROM status")
my_result_s = my_cursor.fetchall()
for x in my_result_s:
    a = {f'{x[0]}': f'{x[1]}'}
    tkt_stat.update(a)


@app.route('/ping',methods = ["GET"])
def ping():
        return "Returning GET request response."

@app.route('/formvalidation',methods = ['GET','POST'])
def formvalidation():
    print("yes received post req")
    payload = request.form
    print(payload)
    data = payload['webhookData']
    print(data)
    data4 = json.loads(data)
    data5 = data4['content']
    print(data5)
    x1.append(data5)
    print(x1)

    return {"message": "success"}


coun = 0
@app.route('/webhook',methods = ["POST"])
def webhook():
    payload = request.form
    data = payload['intent']
    data1 = json.loads(data)
    print(type(data1))
    print(data1)
    action = data1['fulfillment']['action']
    parameters = data1['fulfillment']['parameters']

    if action == 'action-verify-user':
        cred = parameters['credentials']
        userid = cred['userid']
        password = cred['password']
        if userid in id_s:
            if password == id_pass[userid]:
                li.insert(0, userid)
                return data1
            else:
                bb = '{"message":"Wrong password, Enter correct password"}'
                cc = json.loads(bb)
                x1[0].update(cc)
                return x1[0]

        else:
            b = '{"message":"Credentials dont match .<br><b>Re-Enter</b> "}'
            c = json.loads(b)
            x1[0].update(c)
            return x1[0]

    if action == 'action-category1':
        level1= parameters['category1']
        li.insert(1,level1)

    if action == 'action-category2':
        level2= parameters['category2']
        li.insert(2,level2)
        li.insert(3,level2)
        li.insert(4,level2)


    if action == 'action-category3':
        level3 = parameters['category3']
        li.insert(3, level3)
        li.insert(4, level3)


    if action == 'action-category4':
        level4 = parameters['category4']
        li.insert(4, level4)


    if action == 'action-file-presence':
        presence = parameters['present']
        li.insert(10, presence)


    if action == 'action-upload-file':

        # print(parameters['file'])
        file = parameters['file'][0]
        filename = file['filename']
        l_url = file['url']
        f_url = 'https://botgo.s3.ap-south-1.amazonaws.com/'
        url = f_url+l_url
        r = requests.get(url)
        b_file = r.content
        file = base64.b64encode(b_file)
        # file_lis.append(file)
        li.insert(11, file)
        # print(li[11])

    if action == 'action-create-ticket-vecv':
        def createtkt():
            # if file is not uploaded
            presence = parameters['present']
            li.insert(10, presence)
            #ticket generation
            tktt = 'VECV-'
            x = random.randint(10000, 99999)
            ftkt = tktt + str(x)
            if ftkt not in tkt:
                return ftkt
            else:
                createtkt()

        ftkt = createtkt()
        li.insert(12,ftkt)



        # insert current date & time

        dt = date.today()
        time = datetime.now().strftime("%H:%M:%S")

        query = """INSERT INTO vecv1(`User Id`,`Level 1`,`Level 2`,`Level 3`,`Level 4`,`Priority`,`Contact Name`,`Mobile No`,`Description`,`Details`,`File Uploaded`, `Files`,`Ticket Id`, `Status`, `Created Date` ,`Created Time`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        var = (li[0], li[1], li[2], li[3], li[4], li[5], li[6],li[7], li[8], li[9],li[10],li[11],li[12],'New', dt, time,)
        my_cursor.execute(query, var)
        conn.commit()
        # insert into status table
        query1 = """INSERT INTO status(`Ticket Id`,`Status`) VALUES (%s,%s)"""
        var1 = (ftkt,'New')
        my_cursor.execute(query1, var1)
        conn.commit()
        a = {"message" : f"Your ticket number is {ftkt}"}
        data1.update(a)
  #send email
        # EMAIL_ADDRESS = os.environ.get('EMAIL_ID')
        # EMAIL_PASSWORD = os.environ.get('EMAIL_PASS')
        # receiver = id_mail[li[0]]
        #
        #
        # msg = MIMEMultipart()
        # msg['Subject'] = 'Vecv Ticket generated details'
        # msg['From'] = EMAIL_ADDRESS
        # msg['To'] = receiver
        #
        # # Create the body of the message (a plain-text and an HTML version).
        # html = f"""\
        # <html>
        #   <head></head>
        #   <body>
        #     <p>Dear User,<br><br><br>
        #        Please find below the latest update of your support ticket.<br><br><br></p>
        #     <p>Incident&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; :  {ftkt} <br><br> </p>
        #     <p>Status  &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; :  New  <br><br></p>
        #     <p>Problem Description &emsp;&emsp;&emsp;&emsp;&emsp;:   {li[8]} - {li[9]} <br><br></p>
        #     <p>Last Updated Comments &emsp;&emsp;&emsp;:  <br><br><br><br></p>
        #     <p>Supported By&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;: VECV   <br><br></p>
        #     <p>Reported By  &emsp;&emsp;&emsp; &emsp;&emsp;&emsp;&emsp;&emsp;:   {li[6]} <br><br></p>
        #     <p>Mail ID &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;    :   {receiver} <br><br></p>
        #     <p> Creation Date &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;: {dt} <br> <br></p>
        #     <p> In case the of any issues or clarifications please call on the Project Udaan  HelpLine # 0124 - 4415990 <br><br></p>
        #     <p> This is an automated mail. Please do not respond on this email <br><br><br></p>
        #     <p>Regards,</p>
        #     <p>Udaan Support Team </p>
        #     <p>Disclaimer</p><br>
        #     <p>This message, including any files transmitted with it, is for the sole use of the intended recipient and
        #     may contain information that is proprietary, confidential, legally privileged or exempt from disclosure under
        #     applicable law. If you are not the intended recipient, please note that any unauthorized use, review, storage,
        #      disclosure or distribution of this message and/or its contents in any form is strictly prohibited. If it appears
        #      that you are not the intended recipient or this message has been forwarded to you without appropriate authority,
        #      please immediately delete this message permanently from your records and notify the sender. Email communication
        #       may be prone to interception and modification during transit by unscrupulous personnel or elements. VE Commercial
        #       Vehicles Limited makes no warranties as to the accuracy or completeness of the information in this message and accepts no
        #        liability for any damages, including without limitation, direct, indirect, incidental, consequential or punitive damages, arising out of or due to use of the information given in this message unless the sender does so with the due authority of VE Commercial Vehicles Limited</p>
        #   </body>
        #    </html> """
        #
        #
        # # Record the MIME types of both parts - text/plain and text/html.
        #
        # part = MIMEText(html, 'html')
        #
        # # Attach parts into message container.
        # # According to RFC 2046, the last part of a multipart message, in this case
        # # the HTML message, is best and preferred.
        #
        # msg.attach(part)
        #
        # with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
        #     smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        #
        #     smtp.send_message(msg)
        return data1

    if action == 'action-verify-ticket':
        try:
            my_cursor.execute("SELECT * FROM vecv1")
            my_tkt = my_cursor.fetchall()
            for x in my_tkt:
                tkt.append(x[14])
                u_t = {f'{x[0]}': f'{x[12]}'}
                i_d = {f'{x[12]}': f'{x[8]}'}
                i_de = {f'{x[12]}': f'{x[9]}'}
                i_dt = {f'{x[12]}': f'{x[14]}'}

                id_tkt.append(u_t)
                id_det.update(i_d)
                id_des.update(i_de)
                id_cre.update(i_dt)

            my_cursor.execute("SELECT * FROM status")
            my_result_s = my_cursor.fetchall()
            for x in my_result_s:
                a = {f'{x[0]}': f'{x[1]}'}
                tkt_stat.update(a)
            ftkt = parameters['ticketno']
            details = id_det[ftkt]
            desc = id_des[ftkt]
            status = tkt_stat[ftkt]
            created = id_cre[ftkt]
            up_crow = {'message': 'Here is the ticket status for ticket No. {previousValue:383}', 'userInput': False,
                       'fulfillment': {'action': 'action-verify-ticket', 'parameters': {'ticketno': 'VECV-98206'},
                                       'previousIntent': 383}, 'id': 384, "metadata": {"templateId": 12, "payload": [
                    {"header": {"overlayText": "Comments:", "imgSrc": None}, "title": f"{ftkt}: {details}",
                     "titleExt": f"Status: {status}", "subtitle": f"Created at : {created}.", "description": f"{desc}",
                     'buttons': [{'name': 'Restart Chat', 'action': {'type': 'button', 'trigger': 90909}}]}]}}
            return up_crow
        except:
            up_cr = {'message':'Ticket not found!',"id": 383,"userInput": True,"trigger": 384}
            return up_cr


    if action == 'action-verify-dealer':
        dtkt_lis = []
        crousal = []
        dealerid = parameters['dealerid']
        if dealerid in id_s:
            for x in id_tkt:
                if dealerid in x.keys():
                    dtkt_lis.append(x[f'{dealerid}'])
            for i in dtkt_lis:
                details = id_det[i]
                desc = id_des[i]
                created = id_cre[i]
                status = tkt_stat[i]
                if status != 'Proposed Solution' and status != 'Confirm':
                    crousa = {"header": {"overlayText": f"Comments:{details}", "imgSrc": None}, "title": f"{i} ",
                              "titleExt": f"Status: {status}", "subtitle": f"Created at : {created}.",
                              "description": f"{desc}",
                              'buttons': [{'name': 'Restart Chat', 'action': {'type': 'button', 'trigger': 90909}}]}
                    crousal.append(crousa)
                    up_crow = {"message": "These are the all pending tickets", "userInput": False,
                           "fulfillment": {"action": "action-verify-ticket",
                                           "parameters": {"dealerid": "{previousValue:382}"}, "previousIntent": 383},
                           "id": 384, "metadata": {"templateId": 12, "payload": crousal}}
                else:
                    pass
            print(up_crow)
            return up_crow


        else:
            a = '{"message": "Wrong input,Enter ID again.","id": 382,"userInput": true,"trigger": 3820}'
            b = json.loads(a)
            return b






    return data1



@app.route('/formsubmit',methods = ['POST'])
def formsubmit():
    payload = request.form
    data = payload['submittedData']
    data4 = json.loads(data)
    name = data4['ContactName']
    contact = data4['MobileNo']
    title = data4['Description']
    desc = data4['Details']
    # insert priority acc to level4
    upper = li[4].upper()
    li.insert(5, lev_pri[upper])
    li.insert(6,name)
    li.insert(7,contact)
    li.insert(8,title)
    li.insert(9,desc)
    li.insert(10, None)
    li.insert(11, None)
    li.insert(12, None)
    li.insert(13, None)

    return {"message":"success"}




if __name__ == '__main__':
    app.run()
