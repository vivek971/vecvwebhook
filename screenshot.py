# Screenshot with floating option
import pyautogui
import tkinter as tk
from tkinter.filedialog import *

root = tk.Tk()

canvas1 = tk.Canvas(root, width= 100, height= 100)
canvas1.pack()

def takeScreenshot():
    myScreenshot = pyautogui.screenshot()
    save_path = asksaveasfilename()
    myScreenshot.save(save_path+"_screenshot.png")

myButton = tk.Button(text="Shot", command= takeScreenshot, font =3)
canvas1.create_window(50,50,window=myButton)

root.mainloop()

# Normal screenshot without any floating canvas/button

# import pyautogui
# import cv2
# import numpy as np
#
# image = pyautogui.screenshot()
# image = cv2.cvtColor(np.array(image),cv2.COLOR_RGB2BGR)
#
# # writing it to the disk using opencv
# cv2.imwrite("/home/vivek/Pictures/Screenshots/image1.png", image)
